# Pdf Mod UI Catalan Translation.
# Copyright (C) 2009 Free Software Foundation, Inc.
# This file is distributed under the same license as the PDF Mod package.
# Andreu Correa Casablanca <castarco@gmail.com>, 2009.
# Jordi Mas i Hernàndez <jmas@softcatala.org>, 2011
#
msgid ""
msgstr ""
"Project-Id-Version: Pdf Mod 0.5\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-07-10 01:50+0200\n"
"PO-Revision-Date: 2011-07-10 01:51+0200\n"
"Last-Translator: Andreu Correa Casablanca <castarco@gmail.com >\n"
"Language-Team: Català <gnome@llistes.softcatala.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../data/pdfmod.desktop.in.in.h:1
msgid "PDF Modifier"
msgstr "Modificador de documents PDF"

#: ../data/pdfmod.desktop.in.in.h:2
msgid "Remove, extract, and rotate pages in PDF documents"
msgstr "Suprimiu, extraieu i capgireu pàgines de documents PDF"

#: ../src/PdfMod/Pdf/Actions/ExportImagesAction.cs:138
#: ../src/PdfMod/Pdf/Page.cs:36
#, csharp-format
msgid "Page {0}"
msgstr "Pàgina {0}"

#. Translators: {0} is the # of pages, {1} is a translated string summarizing the pages, eg "page 1"
#: ../src/PdfMod/Pdf/Actions/MoveAction.cs:24
#, csharp-format
msgid "Move {1}"
msgid_plural "Move {1}"
msgstr[0] "Mou-ne {1}"
msgstr[1] "Mou-ne {1}"

#: ../src/PdfMod/Pdf/Actions/MoveAction.cs:31
msgid "Error trying to unmove pages"
msgstr "S'ha produït un error en provar de desfer un moviment de pàgines"

#: ../src/PdfMod/Pdf/Actions/MoveAction.cs:41
msgid "Error trying to move pages"
msgstr "S'ha produït un error en provar de moure pàgines"

#: ../src/PdfMod/Gui/Actions.cs:65
msgid "_File"
msgstr "_Fitxer"

#: ../src/PdfMod/Gui/Actions.cs:66
msgid "Open a document"
msgstr "Obre un document"

#: ../src/PdfMod/Gui/Actions.cs:67
msgid "Save changes to this document, overwriting the existing file"
msgstr ""
"Desa els canvis realitzats al document, se sobreescriurà el fitxer existent"

#: ../src/PdfMod/Gui/Actions.cs:68
msgid "Save this document to a new file"
msgstr "Desa el document en un fitxer nou"

#: ../src/PdfMod/Gui/Actions.cs:69
msgid "Recent _Files"
msgstr "_Fitxers recents"

#: ../src/PdfMod/Gui/Actions.cs:70
msgid "_Insert From..."
msgstr "_Insereix des de..."

#: ../src/PdfMod/Gui/Actions.cs:70
msgid "Insert pages from another document"
msgstr "Insereix pàgines d'un altre document"

#: ../src/PdfMod/Gui/Actions.cs:73
msgid "_Edit"
msgstr "_Edita"

#: ../src/PdfMod/Gui/Actions.cs:78
msgid "Rotate Left"
msgstr "Gira a l'esquerra"

#: ../src/PdfMod/Gui/Actions.cs:78
msgid "Rotate left"
msgstr "Gira a l'esquerra"

#: ../src/PdfMod/Gui/Actions.cs:79
msgid "Rotate Right"
msgstr "Gira a la dreta"

#: ../src/PdfMod/Gui/Actions.cs:79
msgid "Rotate right"
msgstr "Gira a la dreta"

#: ../src/PdfMod/Gui/Actions.cs:82
msgid "Select Odd Pages"
msgstr "Selecciona les pàgines senars"

#: ../src/PdfMod/Gui/Actions.cs:83
msgid "Select Even Pages"
msgstr "Selecciona les pàgines parelles"

#: ../src/PdfMod/Gui/Actions.cs:84
msgid "Select Matching..."
msgstr "Selecciona per coincidència..."

#: ../src/PdfMod/Gui/Actions.cs:85
msgid "_Invert Selection"
msgstr "_Inverteix la selecció"

#: ../src/PdfMod/Gui/Actions.cs:87
msgid "_View"
msgstr "_Visualitza"

#: ../src/PdfMod/Gui/Actions.cs:90
msgid "Open in Viewer"
msgstr "Obre en el visualitzador"

#: ../src/PdfMod/Gui/Actions.cs:90
msgid "Open in viewer"
msgstr "Obre en el visualitzador"

#: ../src/PdfMod/Gui/Actions.cs:92 ../src/PdfMod/Gui/BookmarkView.cs:346
msgid "_Bookmarks"
msgstr "_Pàgines d'interès"

#: ../src/PdfMod/Gui/Actions.cs:93
msgid "_Add Bookmark"
msgstr "_Afegeix una pàgina d'interès"

#: ../src/PdfMod/Gui/Actions.cs:94
msgid "Re_name Bookmark"
msgstr "Canvia el _nom d'una pàgina d'interès"

#: ../src/PdfMod/Gui/Actions.cs:95
msgid "_Change Bookmark Destination"
msgstr "_Canvia la destinació de la pàgina d'interès"

#. Translators: {0} is available for you to use; contains the number of bookmarks
#: ../src/PdfMod/Gui/Actions.cs:96 ../src/PdfMod/Gui/BookmarkView.cs:383
#, csharp-format
msgid "_Remove Bookmark"
msgid_plural "_Remove Bookmarks"
msgstr[0] "_Suprimeix la pàgina d'interès"
msgstr[1] "_Suprimeix les pàgines d'interès"

#: ../src/PdfMod/Gui/Actions.cs:97
msgid "_Edit Bookmarks"
msgstr "_Edita les pàgines d'interès"

#: ../src/PdfMod/Gui/Actions.cs:99
msgid "_Help"
msgstr "A_juda"

#: ../src/PdfMod/Gui/Actions.cs:100
msgid "_Contents"
msgstr "_Continguts"

#: ../src/PdfMod/Gui/Actions.cs:107
msgid "_Add"
msgstr "_Afegeix"

#: ../src/PdfMod/Gui/Actions.cs:108
msgid "_Remove"
msgstr "_Suprimeix"

#: ../src/PdfMod/Gui/Actions.cs:111
msgid "View and edit the title, keywords, and more for this document"
msgstr ""
"Visualitza el document i edita'n el títol, les paraules clau i d'altres dades"

#: ../src/PdfMod/Gui/Actions.cs:113
msgid "Toolbar"
msgstr "Barra d'eines"

#: ../src/PdfMod/Gui/Actions.cs:114
msgid "Bookmarks"
msgstr "Pàgines d'interès"

#: ../src/PdfMod/Gui/Actions.cs:115
msgid "Fullscreen"
msgstr "Pantalla completa"

#: ../src/PdfMod/Gui/Actions.cs:186
#, csharp-format
msgid "Export Image"
msgid_plural "Export {0} Images"
msgstr[0] "Exporta la imatge"
msgstr[1] "Exporta {0} imatges"

#: ../src/PdfMod/Gui/Actions.cs:189
#, csharp-format
msgid "Save image from the selected pages to a new folder"
msgid_plural "Save {0} images from the selected pages to a new folder"
msgstr[0] "Desa la imatge de les pàgines seleccionades a una carpeta nova"
msgstr[1] ""
"Desa les {0} imatges de les pàgines seleccionades a una carpeta nova"

#: ../src/PdfMod/Gui/Actions.cs:196
msgid "_Undo"
msgstr "_Desfés"

#: ../src/PdfMod/Gui/Actions.cs:197
#, csharp-format
msgid "Undo {0}"
msgstr "Desfés {0}"

#: ../src/PdfMod/Gui/Actions.cs:201
msgid "_Redo"
msgstr "_Refés"

#: ../src/PdfMod/Gui/Actions.cs:202
#, csharp-format
msgid "Redo {0}"
msgstr "Refés {0}"

#: ../src/PdfMod/Gui/Actions.cs:210
#, csharp-format
msgid "Remove Page"
msgid_plural "Remove {0} Pages"
msgstr[0] "Suprimeix la pàgina"
msgstr[1] "Suprimeix {0} pàgines"

#: ../src/PdfMod/Gui/Actions.cs:213
#, csharp-format
msgid "Remove the selected page"
msgid_plural "Remove the {0} selected pages"
msgstr[0] "Suprimeix la pàgina seleccionada"
msgstr[1] "Suprimeix les pàgines {0} seleccionades"

#: ../src/PdfMod/Gui/Actions.cs:216
#, csharp-format
msgid "Extract Page"
msgid_plural "Extract {0} Pages"
msgstr[0] "Extreu la pàgina"
msgstr[1] "Extreu les {0} pàgines"

#: ../src/PdfMod/Gui/Actions.cs:219
#, csharp-format
msgid "Extract the selected page"
msgid_plural "Extract the {0} selected pages"
msgstr[0] "Extreu la pàgina seleccionada"
msgstr[1] "Extreu les {0} pàgines seleccionades"

#: ../src/PdfMod/Gui/Actions.cs:229 ../src/PdfMod/Gui/Actions.cs:316
msgid "Select PDF"
msgstr "Selecciona el PDF"

#: ../src/PdfMod/Gui/Actions.cs:258
msgid "Unable to Save Document"
msgstr "No s'ha pogut desar el document"

#: ../src/PdfMod/Gui/Actions.cs:264
msgid "Save as..."
msgstr "Anomena i desa..."

#: ../src/PdfMod/Gui/Actions.cs:333 ../src/PdfMod/Gui/Client.cs:308
msgid "Error Loading Document"
msgstr "S'ha produït un error en carregar el document"

#: ../src/PdfMod/Gui/Actions.cs:334 ../src/PdfMod/Gui/Client.cs:309
#, csharp-format
msgid "There was an error loading {0}"
msgstr "S'ha produït un error en carregar {0}"

#: ../src/PdfMod/Gui/Actions.cs:510
msgid "Error opening help"
msgstr "S'ha produït un error en obrir l'ajuda"

#: ../src/PdfMod/Gui/Actions.cs:511
msgid "Would you like to open PDF Mod's online documentation?"
msgstr "Voleu obrir l'ajuda en línia del PDF Mod?"

#: ../src/PdfMod/Gui/Actions.cs:532
msgid "Visit Website"
msgstr "Visita el lloc web"

#: ../src/PdfMod/Gui/Actions.cs:534
msgid "Primary Development:"
msgstr "Desenvolupament inicial:"

#: ../src/PdfMod/Gui/Actions.cs:537
msgid "Contributors:"
msgstr "Contribuïdors:"

#. Translators: {0} and {1} are the years the copyright assertion covers; put into
#. variables so you don't have to re-translate this every year
#: ../src/PdfMod/Gui/Actions.cs:558
#, csharp-format
msgid ""
"Copyright {0} Novell Inc.\n"
"Copyright {1} Other PDF Mod Contributors"
msgstr ""
"Copyright {0} Novell Inc.\n"
"Copyright {1} Altres col·laboradors del PDF Mod"

#: ../src/PdfMod/Gui/Actions.cs:561
msgid "translator-credits"
msgstr ""
"Andreu Correa Casablanca <castarco@gmail.com>\n"
"Jordi Mas i Hernàndez <jmas@softcatala.org>"

#. Add it to the PDF document
#: ../src/PdfMod/Gui/BookmarkView.cs:90
msgid "New bookmark"
msgstr "Pàgina d'interès nova"

#: ../src/PdfMod/Gui/BookmarkView.cs:113
msgid "Add Bookmark"
msgstr "Afegeix una pàgina d'interès nova"

#. Translators: {0} is available for you to use; contains the number of bookmarks
#: ../src/PdfMod/Gui/BookmarkView.cs:137
#, csharp-format
msgid "Remove Bookmark"
msgid_plural "Remove Bookmarks"
msgstr[0] "Suprimeix la pàgina d'interès"
msgstr[1] "Suprimeix les pàgines d'interès"

#: ../src/PdfMod/Gui/BookmarkView.cs:285 ../src/PdfMod/Gui/BookmarkView.cs:323
msgid "Rename Bookmark"
msgstr "Canvia el nom de la pàgina d'interès"

#: ../src/PdfMod/Gui/BookmarkView.cs:417
#, csharp-format
msgid "Bookmark links to page {0}"
msgstr "La pàgina d'interès enllaça a la pàgina {0}"

#: ../src/PdfMod/Gui/Client.cs:86
msgid "PDF Mod"
msgstr "PDF Mod"

#: ../src/PdfMod/Gui/Client.cs:207
msgid "Save the changes made to this document?"
msgstr "Voleu desar els canvis realitzats en aquest document?"

#: ../src/PdfMod/Gui/Client.cs:210
msgid "Close _Without Saving"
msgstr "Tanca _sense desar"

#: ../src/PdfMod/Gui/Client.cs:240
#, csharp-format
msgid "Continue, opening {0} document in separate windows?"
msgid_plural "Continue, opening all {0} documents in separate windows?"
msgstr[0] "Continua i obre el document {0} en una finestra separada?"
msgstr[1] "Continua i obre els documents {0} en finestres separades?"

#: ../src/PdfMod/Gui/Client.cs:244
msgid "Open _First"
msgstr "Obre _primer"

#: ../src/PdfMod/Gui/Client.cs:245
msgid "Open _All"
msgstr "Obre'ls _tots"

#: ../src/PdfMod/Gui/Client.cs:276
msgid "Loading document..."
msgstr "S'està carregant el document..."

#. Translators: this string is used to show current/original file size, eg "2 MB (originally 1 MB)"
#: ../src/PdfMod/Gui/Client.cs:338
#, csharp-format
msgid "{0} (originally {1})"
msgstr "{0} (originalment {1})"

#: ../src/PdfMod/Gui/Client.cs:343
#, csharp-format
msgid "{0} page"
msgid_plural "{0} pages"
msgstr[0] "{0} pàgina"
msgstr[1] "{0} pàgines"

#: ../src/PdfMod/Gui/Client.cs:364
msgid "Document is Encrypted"
msgstr "El document és encriptat"

#: ../src/PdfMod/Gui/Client.cs:365
msgid "Enter the document's password to open it:"
msgstr "Introdueix la contrasenya del document per obrir-lo:"

#: ../src/PdfMod/Gui/Client.cs:395
msgid "PDF Documents"
msgstr "Documents PDF"

#: ../src/PdfMod/Gui/Client.cs:396
msgid "All Files"
msgstr "Tots els fitxers"

#: ../src/PdfMod/Gui/SelectMatchingBox.cs:41
msgid "Select Matching"
msgstr "Selecciona per coincidència"

#: ../src/PdfMod/Gui/MetadataEditorBox.cs:62
msgid "_Title:"
msgstr "_Títol:"

#: ../src/PdfMod/Gui/MetadataEditorBox.cs:63
msgid "_Author:"
msgstr "_Autor:"

#: ../src/PdfMod/Gui/MetadataEditorBox.cs:64
msgid "_Keywords:"
msgstr "Paraules _clau:"

#: ../src/PdfMod/Gui/MetadataEditorBox.cs:65
msgid "_Subject:"
msgstr "_Assumpte:"

#: ../src/PdfMod/Gui/MetadataEditorBox.cs:85
msgid "_Revert Properties"
msgstr "_Reverteix les propietats"

#: ../src/PdfMod/Gui/MetadataEditorBox.cs:86
msgid "Change the document's properties back to the original values"
msgstr ""
"Canvia les propietats del document per tornar-les als seus valors originals"

#: ../src/PdfMod/Gui/MetadataEditorBox.cs:89
msgid "_Close"
msgstr "_Tanca"

#: ../src/PdfMod/Gui/MetadataEditorBox.cs:90
msgid "Hide the document's properties"
msgstr "Oculta les propietats del document"

#~ msgid "Error trying to remove pages from document"
#~ msgstr "S'ha produït un error en provar de suprimir pàgines del document"
